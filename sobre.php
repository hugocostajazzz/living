<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Living Interiores | Quem Somos</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <!-- Titulo da Seção-->
   <section class="titulo-secao">
        <h1>Quem Somos</h1>
   </section>
   <!-- Seção Sobre-->
    <section class="sobre">
        <div class="container-fluid pl-0 pr-0">
            <!--Primeira linha-->
            <div class="row">
            <!--Primeira Coluna-->
                <div class="col-md-6 ">
                    <h2>Nossa Especialidade é montar</h2>
                    <h3 class="h2">A casa perfeita para você</h3>
                    <p class="bolder">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Voluptate at in rem deleniti eius nulla neque non molestias, fugit, quam sit reprehenderit nostrum, quo corrupti impedit ipsa natus. Delectus, doloremque.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore aperiam vitae dicta, voluptate, deleniti, expedita debitis est pariatur numquam optio hic? Aliquam eaque adipisci dolorum ab ad iste in quod.</p>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Itaque accusamus eligendi nostrum hic sed quidem, beatae atque voluptates eum minima, doloremque reiciendis deserunt reprehenderit est cum assumenda quam dolores ab?</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officia debitis tempore eum odio magnam quas numquam a sed ipsum temporibus nobis possimus dolore accusamus soluta, vitae architecto fugit repellat aliquam.</p>
                </div>
                <!--Segunda Coluna-->
                <div class="col-md-6 pr-0">
                    <img class="img-sobre img-fluid" src="assets/imgs/img-sobre.png" alt="">
                </div>
            </div>
            <!--Segunda linha-->
            <div class="row">
                <!--Primeira Coluna-->
                <div class="col-md-6 pl-0">
                    <img class="img-sobre img-fluid" src="assets/imgs/Quem-Somos.png" alt="">
                </div>
                <!--Segunda Coluna-->
                <div class="col-md-6">
                    <h2>Nossa idéia é levar o melhor</h2>
                    <h3 class="h2">Para sua casa ficar perfeita</h3>
                    <p class="bolder">Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe, facilis! Aliquid a omnis inventore iure debitis animi repudiandae, architecto quia dolor sint illum ad, sit maiores provident! Et, quibusdam consequuntur
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum rem magnam vero vel sequi, adipisci excepturi suscipit possimus nam doloremque aspernatur magni porro, doloribus autem temporibus architecto exercitationem soluta voluptatum.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi doloremque a cupiditate deserunt odit consequatur, ipsa officiis soluta sit autem temporibus quas totam enim tempora officia modi cumque quaerat animi?</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ab, iste veniam error quo, ipsam officia laudantium nemo temporibus atque magni aut libero rerum? Iusto atque et voluptatibus in dolor totam.</p>
                </div>
            </div>
        </div>
        <!--Video Institucional-->
        <div class="video-institucional">
            <iframe width="100%" height="555" src="https://www.youtube.com/embed/9NRDamu-l_k" frameborder="0"        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>  
    </section>

    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
