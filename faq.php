<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Living Interiores | Perguntas Frequentes</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <!-- Titulo da Seção-->
   <section class="titulo-secao">
        <h1>Perguntas Frequentes</h1>
   </section>
    <section class="perguntas">
        <span class="sub-perguntas">Abaixo tiramos as dúvidas mais frequentes dos nossos clientes. Confira.</span>
        <div class="cont-perguntas">
            <div class="faq-conteudo">
                <h2>Procedimento de compra. Como realizar?</h2>
                <ul class="list-unstyled">
                    <li>1. Adicione quantos produtos quiser ao carrinho</li>
                    <li>2. Após estar com todos os produtos que deseja no carrinho, confira as quantidades e clique em CONCLUIR</li>
                    <li>3. Se for cadastrado, digite seu login e senha. Se não for, informe o CEP e clique em AVANÇAR para fazer o cadastro antes de concluir a compra.</li>
                    <li>4. Escolha o endereço de entrega e uma das modalidades de frete disponíveis. Clique em CONTINUAR</li>
                    <li>5. O próximo passo é escolher a forma de pagamento. Escolha e clique em AVANÇAR.</li>
                    <li>6. Escolha a forma de pagamento. Você será levado ao ambiente de pagamento para pagamentos em boleto/transferência bancária ou Cielo para pagamento com cartão de crédito. Dependendo do cartão, ainda será levado ao ambiente do banco para confirmar a autenticidade do usuário.</li>
                    <li>7. Pedido finalizado. O sitema vai gerar um número para que você possa acompanhar o status de seu pedido pelo site, através</li>
                    <li>Se a dúvida persistir, entre em contato conosco:<br><a href="">Chat</a> ou pelo Fale Conosco ou pelo telefone <a href="">81 3325-4455.</a></li>
                </ul>
            </div>
            <div class="faq-conteudo">
                <h2>Como sei que concluí meu pedido?</h2>
                <p>Seu pedido está concluído quando o sistema gerar um NÚMERO para acompanhamento. Você pode monitorar a movimentação - desde a aprovação até o despacho  - no próprio site, clicando em MEUS PEDIDOS, no canto superior direito da página, ou pela Central do Cliente. Você também receberá um e-mail automático, informando a cada passo o status do seu pedido.</p>
            </div>
            <div class="faq-conteudo">
                <h2>Como funciona o pagamento?</h2>
                <p>Você pode pagar suas compras com cartão de crédito, boleto bancário (à vista, com 5% de desconto) ou por transferência bancária (apenas para clientes do Banco Itaú). Saiba mais clicando em FORMAS DE PAGAMENTO, no rodapé do site</p>
            </div>
            <div class="faq-conteudo">
                <h2>Como funciona a entrega?</h2>
                <p>A <strong>LIVING INTERIORES</strong> trabalha com frota própria para entregas na cidade do Recife, sem cobrança de taxa para compras acima de R$ 250,00. Para os municípios da RMR, interior e outros estados, clique em FORMAS DE ENTREGA, no rodapé do site. Em breve estará disponível também o frete via transportadora.</p>
            </div>
        </div>
    </section>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
