<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Living Interiores | Como Funciona</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <!-- Titulo da Seção-->
   <section class="titulo-secao">
        <h1>Como Funciona</h1>
   </section>
    <section class="como-funciona">
        <span class="sub-como-funciona">Veja como é fácil fazer o seu pedido. <br>Selecione o produto, coloque-o no carrinho, insira seu CEP e pague em até 12x no cartão. Confira.</span>
        <div class="cont-como-funciona">
            <div class="como-funciona-conteudo">
                <h2>1. Escolha o produto</h2>
            </div>
            <div class="como-funciona-conteudo">
                <h2>2. Coloque o produto no carrinho</h2>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nisi, deleniti officia? Et dolorem, quam error ullam, distinctio sapiente rerum corrupti, suscipit magni fuga iure atque corporis sunt assumenda! Asperiores, repellat!.</p>
            </div>
            <div class="como-funciona-conteudo">
                <h2>3. Insira seu CEP</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Culpa cupiditate fugit quod repellat adipisci! Natus, numquam similique ullam accusantium explicabo earum voluptas dolore fugiat dolorem animi deserunt fuga doloremque error!</p>
            </div>
            <div class="como-funciona-conteudo">
                <h2>4. Escolha a forma de pagamento</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nobis enim dolor distinctio eius, voluptatem minima similique, provident undede veritatis maiores alias quaerat voluptatibus repellat ducimus natus, voluptates officiis ad numquam.loremde veritatis maiores alias quaerat voluptatibus repellat ducimus natus, voluptates officiis ad numquam.lorem veritatis maiores alias quaerat voluptatibus repellat ducimus natus, voluptates officiis ad numquam.lorem </p>
            </div>
        </div>
    </section>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
