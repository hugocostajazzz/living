<!DOCTYPE html>
<html lang="pt-br">
  <head>
      <?php require_once('templates/chamada.php');?>
      <title>Living Interiores | Loja Virtual</title>
  </head>
  <body style="overflow-x: hidden">
    <!-- HEADER -->
    <?php require 'templates/header.php' ?>
    <!-- Submenu -->
    <?php require 'templates/submenu.php'?>
    <!--Chamada Carrosel -->
    <div id="carrosel-loja" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="assets/imgs/banner_lojas.png" class="carrosel-loja" alt="...">
            </div>
            <div class="carousel-item">
                <img src="assets/imgs/banner_home.png" class="carrosel-loja" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carrosel-loja" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carrosel-loja" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- Chamada da loja-->
    <section class="categoria-interno">
        <div class="cont-categoria">
            <div class="card-categoria">
                <img src="assets/imgs/lançamento_banner1.png" alt="">
                <button>Lançamentos</button>
                <div class="filtro"></div>
            </div>
            <div class="card-categoria">
                <img src="assets/imgs/lançamento_banner2.png" alt="">
                <button>Ver Produto</button>
                <div class="filtro"></div>
            </div>
        </div>
    </section>
    <!-- Destaque de produtos-->
    <section class="destaque-loja">
      <div class="titulo-destaque">
          <h1>Produtos em Destaque</h1>
      </div>
      <div class="cont-produto">
        <!-- Item loja-->
          <article class="card-produto">
              <aside class="novo">
                  <img src="assets/icons/novo.png" alt="">
              </aside>
              <div class="prod-home-img">
                  <img src="assets/imgs/movel1.png" alt="">
              </div>
              <div class="prod-home-txt">
                  <h1>Poltrona</h1>
                  <p>detalhe e informações do produto</p>
                  <span>SKU 9702133</span>
                  <img src="assets/icons/estrelas.jpg" alt="">
              </div>
              <div class="preco-prod-home">
                  <span>R$ 7.849,00</span>
                  <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
              </div>
          </article>
          <!-- Item loja-->
          <article class="card-produto">
              <aside class="novo">
                  <img src="assets/icons/novo.png" alt="">
              </aside>
              <div class="prod-home-img">
                  <img src="assets/imgs/movel2.png" alt="">
              </div>
              <div class="prod-home-txt">
                  <h1>Poltrona</h1>
                  <p>detalhe e informações do produto</p>
                  <span>SKU 9702133</span>
                  <img src="assets/icons/estrelas.jpg" alt="">
              </div>
              <div class="preco-prod-home">
                  <span>R$ 7.849,00</span>
                  <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
              </div>
          </article>
          <!-- Item loja-->
          <article class="card-produto">
              <aside class="novo">
                  <img src="assets/icons/novo.png" alt="">
              </aside>
              <div class="prod-home-img">
                  <img src="assets/imgs/movel3.png" alt="">
              </div>
              <div class="prod-home-txt">
                  <h1>Poltrona</h1>
                  <p>detalhe e informações do produto</p>
                  <span>SKU 9702133</span>
                  <img src="assets/icons/estrelas.jpg" alt="">
              </div>
              <div class="preco-prod-home">
                  <span>R$ 7.849,00</span>
                  <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
              </div>
          </article>
          <!-- Item loja-->
          <article class="card-produto">
              <aside class="novo">
                  <img src="assets/icons/novo.png" alt="">
              </aside>
              <div class="prod-home-img">
                  <img src="assets/imgs/movel2.png" alt="">
              </div>
              <div class="prod-home-txt">
                  <h1>Poltrona</h1>
                  <p>detalhe e informações do produto</p>
                  <span>SKU 9702133</span>
                  <img src="assets/icons/estrelas.jpg" alt="">
              </div>
              <div class="preco-prod-home">
                  <span>R$ 7.849,00</span>
                  <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
              </div>
          </article>
          <!-- Item loja-->
          <article class="card-produto">
              <aside class="novo">
                  <img src="assets/icons/novo.png" alt="">
              </aside>
              <div class="prod-home-img">
                  <img src="assets/imgs/movel1.png" alt="">
              </div>
              <div class="prod-home-txt">
                  <h1>Poltrona</h1>
                  <p>detalhe e informações do produto</p>
                  <span>SKU 9702133</span>
                  <img src="assets/icons/estrelas.jpg" alt="">
              </div>
              <div class="preco-prod-home">
                  <span>R$ 7.849,00</span>
                  <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
              </div>
          </article>
          <!-- Item loja-->
          <article class="card-produto">
              <aside class="novo">
                  <img src="assets/icons/novo.png" alt="">
              </aside>
              <div class="prod-home-img">
                  <img src="assets/imgs/movel3.png" alt="">
              </div>
              <div class="prod-home-txt">
                  <h1>Poltrona</h1>
                  <p>detalhe e informações do produto</p>
                  <span>SKU 9702133</span>
                  <img src="assets/icons/estrelas.jpg" alt="">
              </div>
              <div class="preco-prod-home">
                  <span>R$ 7.849,00</span>
                  <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
              </div>
          </article>
      </div>
    </section>
    <!--footer-->
    <?php require 'templates/footer.php' ?>
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
  </body>
</html>