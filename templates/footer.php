<footer>
  <div class="footer-top">
      <div class="news-chamada">
          <div class="img-news">
            <img src="assets/icons/carta.png" alt="">
          </div>
          <div class="assine">
            <h4>Assine nossas promoções</h4>
            <span>Receba novidades e descontos excluvivos em seu e-mail</span>
          </div>
      </div>
      <form class="cadastro" action="">
        <div class="input-group">
          <input type="text" placeholder="Cadastre seu e-mail agora">
          <button>Cadastrar</button>
        </div>
      </form>
  </div>
  <div class="footer-mid">
    <div class="coluna">
        <div class="logo-footer">
          <a href="./"><img src="assets/icons/logo.png" alt=""></a>
          <ul class="list-unstyled social-footer">
              <li><a href="https://www.facebook.com/livinginteriores" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
              <li><a  href="https://www.instagram.com/livinginteriores/" target="_blank"><i class="fab fa-instagram"></i></a></li>
          </ul>
        </div>
    </div>
    <div class="coluna">
        <ul class="list-unstyled mapa-site">
            <li>Central de Ajuda</li>
            <li>CNPJ:00.000.000/0001-00</li>
            <li><a href="https://www.google.com.br/maps/place/Living+Interiores/@-8.1095036,-34.8929465,17z/data=!3m1!4b1!4m5!3m4!1s0x7ab1f098253c19b:0x2d3175facfda042b!8m2!3d-8.1095036!4d-34.8907578" target="_blank">Av. Conseleiro Aguiar, 2252</a></li>
            <li><a href="https://www.google.com.br/maps/place/Living+Interiores/@-8.1095036,-34.8929465,17z/data=!3m1!4b1!4m5!3m4!1s0x7ab1f098253c19b:0x2d3175facfda042b!8m2!3d-8.1095036!4d-34.8907578" target="_blank">Boa Viagem</a></li>
            <li><a href="https://www.google.com.br/maps/place/Living+Interiores/@-8.1095036,-34.8929465,17z/data=!3m1!4b1!4m5!3m4!1s0x7ab1f098253c19b:0x2d3175facfda042b!8m2!3d-8.1095036!4d-34.8907578" target="_blank">Recife-PE, 51111-011</a></li>
            <li><a href="tel:8133254455">Fone: 81 <strong>3325-4455</strong></a></li>
            <li><a href="mailto:contato@livinginteriores.com.br">contato@livinginteriores.com.br</a></li>    
        </ul>
    </div>
    <div class="coluna">
        <ul class="list-unstyled mapa-site">
            <li>Institucional</li>
            <li><a href="sobre.php">Sobre a empresa</a></li>
            <li><a href="trabalheConosco.php">Trabalhe conosco</a></li>
            <li><a href="faleConosco.php">Fale conosoco</a></li>
            <li><a href="">Blog</a></li>
        </ul>
    </div>
    <div class="coluna">
        <ul class="list-unstyled mapa-site">
            <li>Políticas</li>
            <li><a href="politicas.php#privacidade">Políticas de privacidade</a></li>
            <li><a href="politicas.php#qualidade">Políticas de qualidade</a></li>
            <li><a href="politicas.php#entrega">Políticas de entrega</a></li>
            <li><a href="politicas.php#seguranca">Política de segurança</a></li>
            <li><a href="politicas.php#troca">Política de troca e devoluções</a></li>
        </ul>
    </div>
    <div class="coluna">
        <ul class="list-unstyled mapa-site">
            <li>Suporte</li>
            <li><a href="comoFunciona.php">Como funciona</a></li>
            <li><a href="faq.php">Perguntas frequentes</a></li>
        </ul>
    </div>

  </div>
  <div class="footer-bottom">
      <div class="seguranca">
          <span>Segurança</span>
          <ul class="list-unstyled">
            <li><img src="assets/icons/ssl.png" alt="Certificado de Segurança SSL"></li>
            <li><img src="assets/icons/googe.png" alt="Navegação segura Google"></li>
          </ul>
      </div>
      <div class="forma-pagamento">
          <span>Formas de Pagamento</span>
          <ul class="list-unstyled">
              <li><img src="assets/icons/visa.png" alt="Cartão Visa"></li>
              <li><img src="assets/icons/master.png" alt="Cartão Master"></li>
              <li><img src="assets/icons/elo.png" alt="Elo"></li>
              <li><img src="assets/icons/america.png" alt="American Express"></li>
              <li><img src="assets/icons/descover.png" alt="Descover"></li>
              <li><img src="assets/icons/club.png" alt="Club"></li>
              <li><img src="assets/icons/aura.png" alt="Aura"></li>
              <li><img src="assets/icons/jcb.png" alt="JCB"></li>
              <li><img src="assets/icons/boleto.png" alt="Boleto bancário"></li>
          </ul>
      </div>
      <div class="jazzz">
        <span>Desenvolvido</span>
          <a href="http://jazzz.com.br" target="_blank"> <img src="assets/icons/jazzz.jpg" title="Desenvolvido orgulhosamente pela Jazzz Agência Digital." alt=""></a>
      </div>
  </div>
</footer>
<?php require 'templates/modal.php' ?>