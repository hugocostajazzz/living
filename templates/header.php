<header>
  <div class="cont-header">
      <div class="logo">
          <a href="./"><img src="assets/icons/logo.png" alt=""></a> 
            <img class="icon" onclick="myFunction()" src="assets/icons/menu.png" alt="">
      </div>
    <nav>
        <ul class="list-unstyled" id="nav">
            <li><a href="./">Home</a></li>
            <li><a href="sobre.php">Sobre</a></li>
            <li><a href="loja.php">Loja Virtual</a></li>
            <li><a href="">Blog</a></li>
            <li><a href="faleConosco.php">Contato</a></li>
            <ul class="list-unstyled mobile">
                <li><a href="https://www.instagram.com/livinginteriores/" target="_blank"><img src="assets/icons/insta.png" alt=""></a></li>
                <li><a href="#"><img data-toggle="modal" data-target="#modal-login" src="assets/icons/user.png" alt=""></a></li>
                <li><a href="carrinho.php"><img class="loja-icon"  src="assets/icons/carrinho.png" alt=""></a></li>
            </ul>
        </ul>
       
    </nav>
    <ul class="list-unstyled social">
        <li><a href="https://www.instagram.com/livinginteriores/" target="_blank"><img src="assets/icons/insta.png" alt=""></a></li>
        <li><a data-toggle="modal" data-target="#modal-login"><img src="assets/icons/user.png" alt=""></a></li>
        <li><a href="carrinho.php"><img class="loja-icon"  src="assets/icons/carrinho.png" alt=""></a></li>
    </ul>
  </div>
</header>

<?php require 'templates/modal.php' ?>