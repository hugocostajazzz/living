


<!-- Modal -->
<div class="modal fade modal-login" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="modal-loginTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
          <div class="title-group">
            <img src="assets/icons/logo.png" alt="">
            <div class="title-modal">
                <img src="assets/icons/substract.png" alt="">
                <h5 class="modal-title" id="modal-loginTitle"> Faça seu login</h5>
                <img src="assets/icons/substract.png" alt="">
            </div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
      </div>
      <div class="modal-body">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-md-6">
                      <h6>Fazer login</h6>
                      <form action="">
                        <input require type="email" name="nome" placeholder="*Email">
                        <input require type="password" name="email" placeholder="*Senha">
                        <a href="" class="small">Esqueci minha senha</a>
                        <button>Entrar</button>
                      </form>
                  </div>
                  <div class="col-md-6 cadastre">
                      <h6>Ainda não sou cadastrado</h6>
                      <p>Se você ainda não é Cadastrado no Living Interiores aproveite agora, cadastre-se e receba em seu Email 10% de desconto na sua primeira compra!</p>
                      <button>Quero me cadastrar</button>
                      <p>* Para ganhar o desconto, guarde o código que você receberá logo após o cadastro.</p>
                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
</div>