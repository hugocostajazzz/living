<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Living Interiores | Carrinho de Compras</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <!-- Titulo da Seção-->
   <section class="titulo-secao">
        <h1>Carrinho de Compras</h1>
   </section>
    <section class="carrinho">
       <div class="container">
           <div class="group-button">
               <div class="etapas ativo">01. Carrinho de Compras</div>
               <div class="etapas">02. Identificação</div>
               <div class="etapas">03. Pagamento</div>
               <div class="etapas">04. Confirmação</div>
           </div>
       </div>
       <div class="container-fluid">
           <!-- Linha de titulos do carrinho-->
           <div class="row cont-carrinho">
               <div class="col-sm-6">
                   <h2 class="text-left">Produto</h2>
               </div>
               <div class="col-sm-2">
                   <h2>Preço</h2>
               </div>
               <div class="col-sm-2">
                   <h2>Quant.</h2>
               </div>
               <div class="col-sm-2">
                  <h2>Sub-total</h2>
               </div>
           </div>
           <!--Linha de produtos do carrinho-->
           <div class="row cont-prod">
                <div class="col-sm-6">
                    <div class="produto-carrinho">
                        <div class="img-produto">
                            <img src="assets/imgs/CARRINHO3.png" alt="">
                        </div>
                        <div class="cont-produto">
                            <h3>Nome do produto</h3>
                            <h4>Cor: Branco</h4>
                            <p>Código: 0000-0000</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 alinhar">
                   <span class="d-block d-sm-none">Preço:</span> <span class="valor">R$ 999,99</span>
                </div>
                <div class="col-sm-2 alinhar">
                   <span class="d-block d-sm-none">Quant:</span> <input type="number" value="1">
                </div>
                <div class="col-sm-2 alinhar">
                  <span class="d-block d-sm-none">Sub-total:</span>  <span class="valor">R$ 999,90</span>
                </div>
           </div>
           <div class="row cont-prod">
                <div class="col-sm-6">
                    <div class="produto-carrinho">
                        <div class="img-produto">
                            <img src="assets/imgs/CARRINHO2.png" alt="">
                        </div>
                        <div class="cont-produto">
                            <h3>Nome do produto</h3>
                            <h4>Cor: Branco</h4>
                            <p>Código: 0000-0000</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 alinhar">
                   <span class="d-block d-sm-none">Preço:</span> <span class="valor">R$ 999,99</span>
                </div>
                <div class="col-sm-2 alinhar">
                   <span class="d-block d-sm-none">Quant:</span> <input type="number" value="1">
                </div>
                <div class="col-sm-2 alinhar">
                  <span class="d-block d-sm-none">Sub-total:</span>  <span class="valor">R$ 999,90</span>
                </div>
           </div>
           <div class="row cont-prod">
                <div class="col-sm-6">
                    <div class="produto-carrinho">
                        <div class="img-produto">
                            <img src="assets/imgs/CARRINHO1.png" alt="">
                        </div>
                        <div class="cont-produto">
                            <h3>Nome do produto</h3>
                            <h4>Cor: Branco</h4>
                            <p>Código: 0000-0000</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 alinhar">
                   <span class="d-block d-sm-none">Preço:</span> <span class="valor">R$ 999,99</span>
                </div>
                <div class="col-sm-2 alinhar">
                   <span class="d-block d-sm-none">Quant:</span> <input type="number" value="1">
                </div>
                <div class="col-sm-2 alinhar">
                  <span class="d-block d-sm-none">Sub-total:</span>  <span class="valor">R$ 999,90</span>
                </div>
           </div>
           <!-- Linha Sub-Total -->
           <div class="row sub">
               <div class="col-12">
                   <span>Sub-total:<span>
                   <span>R$ 5.389,80</span>
               </div>
           </div>
           <!-- Linha cupons e frete -->
           <div class="row cont-cupom">
               <div class="col-md-6">
                   <h4>Cupom de Desconto</h4>
                   <span>Digite abaixo seu cupom de desconto.</span>
                   <button>Aplicar</button>
               </div>
               <div class="col-md-6">
                   <h4>Calcular Frete</h4>
                   <span>Informe seu CEP para calcular o valor da entrega</span>
                   <button>Calcular</button>
               </div>
           </div>
           <!-- Linha do total-->
           <div class="row total">
               <div class="col-12">
                    <span>Total</span>
                    <span>R$ 29,70</span>
               </div>
           </div>
           <!-- Linha Cartões e finalizar pedido-->
           <div class="row finalizar">
               <div class="col-md-8">
                   <img src="assets/icons/cards.jpg" alt="">
               </div>
               <div class="col-md-4">
                   <button>Finalizar Pedido</button>
               </div>
           </div>
       </div>
    </section>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
