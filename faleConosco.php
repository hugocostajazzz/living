<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Living Interiores | Fale Conosco</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <!-- Titulo da Seção-->
   <section class="titulo-secao">
        <h1>Fale Conosco</h1>
   </section>
    <section class="fale-conosco">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 info">
                    <p>Para maiores informações, preencha os dados<br> ao lado e em breve retornaremos o seu contato.<br>Se preferir, entre em contato através dos dados abaixo:</p>
                    <a class="email" href="mailto:contato@livinginteriores.com.br">contato@livinginteriores.com.br</a>
                    <address class="mt-3">
                            <ul class="list-unstyled">
                                <li>Endereço Loja Física</li>
                                <li>Av. Conselheiro Aguiar, 2252</li>
                                <li>Boa Viagem - Recife - PE</li>
                                <li>CEP 51111-011</li>
                            </ul>
                        <a class="mapa" href="https://www.google.com/maps/place/Living+Interiores/@-8.1094983,-34.8929465,17z/data=!3m1!4b1!4m5!3m4!1s0x7ab1f098253c19b:0x2d3175facfda042b!8m2!3d-8.1095036!4d-34.8907578?hl=pt-BR" target="_blank">Ver mapa <i class="fas fa-arrow-right"></i></a>
                    </address>
                    <a class="tel" href="">Fone (81) <strong>335-4455</strong></a>
                </div>
                <div class="col-md-6 form">
                    <form action="">
                        <input require type="text" name="nome" placeholder="Digite seu nome">
                        <input require type="email" name="email" placeholder="Digite seu e-mail">
                        <input require type="text" name="tel" placeholder="Digite seu telefone">
                        <textarea require name="mensagem" cols="30" rows="10" placeholder="Digite sua mensagem"></textarea>
                        <div class="btn-enviar d-flex justify-content-end">
                            <button><span>Enviar</span> <img src="assets/icons/seta.png" alt=""></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
