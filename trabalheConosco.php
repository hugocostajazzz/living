<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Living Interiores | Trabalhe Conosco</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <!-- Titulo da Seção-->
   <section class="titulo-secao">
        <h1>Trabalhe Conosco</h1>
   </section>
   <!--Seção trabalhe conosco-->
    <section class="trabalhe-conosco">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 info">
                    <p>Trabalhar conosco, preencha os dados<br> ao lado e em breve retornaremos o seu contato.<br>Se preferir, entre em contato através dos dados abaixo:</p>
                    <a class="email" href="mailto:contato@livinginteriores.com.br">contato@livinginteriores.com.br</a>
                </div>
                <div class="col-md-6 form">
                <!--Formulário de Envio contato-->
                    <form action="">
                        <input require type="text" name="nome" placeholder="Digite seu nome">
                        <input require type="email" name="email" placeholder="Digite seu e-mail">
                        <input require type="text" name="tel" placeholder="Digite seu telefone">
                        <textarea require name="mensagem" cols="30" rows="10" placeholder="Digite sua mensagem"></textarea>
                        <div class="arquivo">
                            <span class="small">Currículo * <strong>Arquivos tipo: .doc, .docx ou .pdf</strong></span>
                            <div class="curriculo-group mt-2">
                                <input type="file" id="file-input" required  class="input-file">
                                <label for="file-input" class="label-file">Escolher Arquivo</label>
                            </div>
                        </div>
                        <div class="btn-enviar d-flex justify-content-end">
                            <button><span>Enviar</span> <img src="assets/icons/seta.png" alt=""></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
