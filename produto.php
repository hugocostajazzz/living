<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Living Interiores | Loja</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <!-- Titulo da Seção-->
   <section class="titulo-secao">
        <h1>Cadeiras e poltronas</h1>
   </section>
    <section class="produto-destaque">
       <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 produto">
                    <img class="img-destaque" src="assets/imgs/produto-destaque.png" alt="">
                    <div class="auxiliador">
                        <img src="assets/icons/estrela.png" alt="">
                        <img src="assets/icons/segura.png" alt="">
                    </div>
                </div>
                <div class="col-md-6 detalhes">
                    <div class="titulo-produto">
                        <h1>Cadeira Living Interiores</h1>
                    </div>
                    <span class="sku">cod. do produto: 0000-0001</span>
                    <ul class="list-unstyled detalhes-prod">
                        <li>Características do produto</li>
                        <li>Marca: Coast Corrente</li>
                        <li>Referência: Mirella</li>
                        <li>Etiqueta:100</li>
                        <li>Tex 145</li>
                        <li>Cor: CREME</li>
                        <li>Composição:100% algodão</li>
                        <li>Unidade de venda: Novelo c/ 1000 mts</li>
                    </ul>
                    <div class="preco">
                        <h3>R$ 990,00</h3>
                        <span>ou em 10x de R$ 99,00</span>
                    </div>
                    <div class="btn-group">
                        <div class="input-qtd">
                            <label for="qtd">Qtd:</label>
                            <input type="number" id="qtd" value="1">
                        </div>
                        <button>Comprar</button>
                    </div>
                    <div class="frete">
                        <label for="frete">Calcular Frete | CEP</label>
                        <div class="group-input">
                            <input type="text" id="frete">
                            <button> ok</button>
                        </div>

                    </div>
                </div>
            </div>
       </div>
       <aside class="opiniao">
           <div class="container-fluid">
               <div class="row">
                   <div class="col-md-6">
                       <h4>Detalhes do produto</h4>
                           <p>O Mirella possui um excelente rendimento com 1000 metros de fio por novelo. É um fio 100% algodão, que recebe um tratamento especial para impedir a pilosidade e garantir uma maciez única aos trabalhos manuais! Especialmente perfeitos para peças e vestuários, pois possui PH neutro - e assim, não causa irritação ao entrar em contato com a pele.
                           </p>
                   </div>
                   <div class="col-md-6">
                       <h4>Opinião dos clientes</h4>
                           <button>Seja o primeiro a avaliar este produto</button>
                   </div>
               </div>
           </div>
       </aside>
       <aside class="relacionados">
           <h4>Produtos Relacionados</h4>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <img src="assets/imgs/PRODUTOD2.png" alt="">
                        <h5>Poltrona Mamade</h5>
                        <span>de R$ 1.500,90</span>
                        <h6>R$999,99</h6>
                        <button>Comprar</button>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <img src="assets/imgs/PRODUTOD2.png" alt="">
                        <h5>Poltrona Mamade</h5>
                        <span>de R$ 1.500,90</span>
                        <h6>R$999,99</h6>
                        <button>Comprar</button>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <img src="assets/imgs/PRODUTOD2.png" alt="">
                        <h5>Poltrona Mamade</h5>
                        <span>de R$ 1.500,90</span>
                        <h6>R$999,99</h6>
                        <button>Comprar</button>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <img src="assets/imgs/PRODUTOD2.png" alt="">
                        <h5>Poltrona Mamade</h5>
                        <span>de R$ 1.500,90</span>
                        <h6>R$999,99</h6>
                        <button>Comprar</button>
                    </div>
                </div>
            </div>
       </aside>
    </section>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
