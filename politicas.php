<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Living Interiores | Políticas</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <!-- Titulo da Seção-->
   <section class="titulo-secao">
        <h1>Nossas Políticas</h1>
   </section>
    <section class="politicas">
            <h2>Informe-se a respeito das nossas políticas de funcionamento e atendimento</h2>
        <div class="container-fluid cont-politicas">
            <div class="row">
                <div class="col-lg-4 col-xl-3">
                    <!--Menu politicas-->
                    <nav class="nav-politicas">
                        <ul class="list-unstyled">
                            <li><a href="#privacidade">Privacidade</a></li>
                            <li><a href="#qualidade">Qualidade</a></li>
                            <li><a href="#entrega">Entrega</a></li>
                            <li><a href="#seguranca">Segurança</a></li>
                            <li><a href="#troca">Troca e Devolução</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-8 col-xl-9">
                    <!-- Item Política-->
                    <article class="item-politica" id="privacidade">
                        <h3>Política de Privacidade</h3>
                        <p>Todas as informações e transações que trafegam pela loja virtual Living Interiores são protegidas por protocolo de segurança padrão SSL com criptografia forte de 128 bits, certificado também pela Thawte, uma das maisores empresas de certificação de segurança no mundo digital, especializada em segurança no mercado virtual mundial.</p>
                    </article>
                    <!-- Item Política-->
                    <article class="item-politica" id="qualidade">
                        <h3>Política de Qualidade</h3>
                        <p>O prazo de entrega dos produtos varia de acordo com o local solicitado para entrega, forma de pagamento escolhida e disponibilidade do produto adquirido, sendo dado sempre ao cliente uma estimativa média de entrega podendo sofrer variações por motivos superiores.</p>
                    </article>
                    <!-- Item Política-->
                    <article class="item-politica" id="entrega">
                        <h3>Política de Entrega</h3>
                        <p>O prazo de entrega dos produtos variam de acordo com o local solicitado para entrega, forma de pagamento escolhida e disponibilidade do produto adquirido, sendo dado sempre ao cliente uma estimativa média de entrega podendo sofrer variações por motivos superiores.</p>
                    </article>
                    <!-- Item Política-->
                    <article class="item-politica" id="seguranca">
                        <h3>Política de Segurança</h3>
                        <p>Toda a nossa estrutura está protegida por sevidores e sistemas de segurança (firewalls), hospedada em Datacenter, seguindo rigorosas normas internacionais de segurança física e lógica. Todas as informações e transações que trafegam pela loja virtual Living Interiores são protegidas por protocolo de segurança padrão SSL, com criptografia forte de 128 bits, certificado também pela Thawte, uma das maiores empresas de certificação de segurança no mundo digital, espcializada em segurança no mercado virtual mundial.</p>
                    </article>
                    <!-- Item Política-->
                    <article class="item-politica" id=troca>
                        <h3>Política de Troca e devoluções</h3>
                        <p>A Living Interiores preocupa com a total satisfação, oferece produtos diferenciados, com excelência e qualidade, buscando sempre atender as necessidades do mercado para trazer o que há de melhor para seus clientes.</p>
                        <p>Em virtude disso, para manter a credibilidade que conquistou e tanto preza, vislumbrando alcançar sempre o maior nível de satisfação dos consumidores, o Armarinho Ivo criou uma Pilítica de Troca e Devolução, a fim de levar amplamente ao conhecimento de seu cliente, o procedimento adotado por esta empresa em decorrência da ocasião.</p>
                        <strong>Regras Gerais:</strong>
                        <p>Caso ocorra necessidade detroca ou devolução, nos casos em que a mesma seja viável, faz-se necessário em primeiro plano, que o cliente entre em contato através do nosso link CONTATO.</p>
                        <p>É imprescindível que se observe as regras que ensejam a devolução, devendo o cliente recusar seu recebimento no momento da entrega, sob pena de não fazer jus à troca.</p>
                    </article>
                </div>
            </div>
        </div>
    </section>

    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
