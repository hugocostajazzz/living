<!DOCTYPE html>
<html lang="pt-br">
<head>
<?php require_once('templates/chamada.php');?>
<title>Living Interiores | Home</title>
</head>
<body style="overflow-x: hidden">
    <!-- HEADER -->
        <?php require 'templates/header.php' ?>
    <!-- PREVIA Carrosel -->
    <div id="carouselExampleFade" class="carousel slide carousel-fade carousel-destaque" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="cont-carrosel">
                    <img src="assets/imgs/banner_home.png" class="img-carrosel" alt="...">
                    <div class="carousel-caption caption-carrosel">
                        <h2>Pra você </h2> <h2>viver bem</h2>
                        <p>Somos uma loja de móveis<br> de alto desempenho.</p>
                        <button class="btn-outline">Linhas de Produtos</button>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <div class="cont-carrosel">
                    <img src="assets/imgs/slide-1.jpg" class="img-carrosel" alt="...">
                    <div class="carousel-caption caption-carrosel">
                        <h2>Living</h2><h2>Interiores</h2>
                        <p>Interiores<br> de alto desempenho.</p>
                        <button class="btn-outline">Linhas de Produtos</button>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- Institucional-->
    <section class="institucional-home">
        <div class="cont-institucional">
            <div class="card-institucional">
               
            </div>
            <div class="card-institucional-txt">
                <h2>Institucional</h2>
                <div class="txt-cont">
                    <h1>Bem vindo a Living</h1>
                    <p>Nisi irure nisi velit quis nulla est cupidatat et velit. Ipsum Lorem tempor id sunt eu. Excepteur laboris eiusmod minim non duis. Tempor ad consequat cillum deserunt cillum in veniam labore est commodo et mollit dolore esse. Esse culpa proident eu ad veniam ex velit fugiat aute aliqua eiusmod deserunt culpa occaecat. Proident reprehenderit cupidatat cillum consectetur excepteur ipsum. In ex eiusmod duis dolore.</p>
                    <p>Adipisicing est elit id labore consequat aliquip fugiat dolore. Velit sint aliqunt cupidatat laborum proident velit.
                    </p>
                    <div class="cont-button">
                        <a href="sobre.php">
                            <button>Conheça a living</button>
                            <div class="seta">
                                <img src="assets/icons/seta_vermelha.png" alt="">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--chamada categoria -->
    <section class="chamada-categoria">
        <div class="cont-chamada">
            <div class="card-categoria">
                <button>Designs exclusivos</button>
            </div>
            <div class="card-categoria">
                <button>Sofisticação</button>
            </div>
            <div class="card-categoria">
                <button>Tendência</button>
            </div>
        </div>
    </section>
    <!--Loja chamada-->
    <section class="produto-home">
        <div class="t-chamada-loja">
            <h1>Produtos em Destaque</h1>
            <a href="loja.php">Ir para loja</a>
        </div>
        <div class="slides-prod">
            <div class="owl-carousel owl-theme">
                <div class="item"><!--Item do carrosel-->
                    <article class="card-produto-home">
                        <aside class="novo">
                            <img src="assets/icons/novo.png" alt="">
                        </aside>
                        <div class="prod-home-img">
                            <a href="produto.php"><img src="assets/imgs/movel1.png" alt=""></a>
                        </div>
                        <div class="prod-home-txt">
                            <h1>Poltrona</h1>
                            <p>detalhe e informações do produto</p>
                            <span>SKU 9702133</span>
                            <img src="assets/icons/estrelas.jpg" alt="">
                        </div>
                        <div class="preco-prod-home">
                            <span>R$ 7.849,00</span>
                            <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
                        </div>
                    </article>
                </div><!--Fim do item-->
                <div class="item"><!--Item do carrosel-->
                    <article class="card-produto-home">
                        <aside class="novo">
                            <img src="assets/icons/novo.png" alt="">
                        </aside>
                        <div class="prod-home-img">
                            <a href="produto.php"> <img src="assets/imgs/movel2.png" alt=""></a>
                        </div>
                        <div class="prod-home-txt">
                            <h1>Sofá</h1>
                            <p>detalhe e informações do produto</p>
                            <span>SKU 9702133</span>
                            <img src="assets/icons/estrelas.jpg" alt="">
                        </div>
                        <div class="preco-prod-home">
                            <span>R$ 7.849,00</span>
                            <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
                        </div>
                    </article>
                </div><!--Fim do item-->
                <div class="item"><!--Item do carrosel-->
                    <article class="card-produto-home">
                        <aside class="novo">
                            <img src="assets/icons/novo.png" alt="">
                        </aside>
                        <div class="prod-home-img">
                            <a href="produto.php"><img src="assets/imgs/movel3.png" alt=""></a>
                        </div>
                        <div class="prod-home-txt">
                            <h1>Cadeira</h1>
                            <p>detalhe e informações do produto</p>
                            <span>SKU 9702133</span>
                            <img src="assets/icons/estrelas.jpg" alt="">
                        </div>
                        <div class="preco-prod-home">
                            <span>R$ 7.849,00</span>
                            <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
                        </div>
                    </article>
                </div><!--Fim do item-->
                <div class="item"><!--Item do carrosel-->
                    <article class="card-produto-home">
                        <aside class="novo">
                            <img src="assets/icons/novo.png" alt="">
                        </aside>
                        <div class="prod-home-img">
                            <a href="produto.php"><img src="assets/imgs/movel1.png" alt=""></a>
                        </div>
                        <div class="prod-home-txt">
                            <h1>Product Name</h1>
                            <p>detalhe e informações do produto</p>
                            <span>SKU 9702133</span>
                            <img src="assets/icons/estrelas.jpg" alt="">
                        </div>
                        <div class="preco-prod-home">
                            <span>R$ 7.849,00</span>
                            <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
                        </div>
                    </article>
                </div><!--Fim do item-->
                <div class="item"><!--Item do carrosel-->
                    <article class="card-produto-home">
                        <aside class="novo">
                            <img src="assets/icons/novo.png" alt="">
                        </aside>
                        <div class="prod-home-img">
                            <a href="produto.php"><img src="assets/imgs/movel3.png" alt=""></a>
                        </div>
                        <div class="prod-home-txt">
                            <h1>Product Name</h1>
                            <p>detalhe e informações do produto</p>
                            <span>SKU 9702133</span>
                            <img src="assets/icons/estrelas.jpg" alt="">
                        </div>
                        <div class="preco-prod-home">
                            <span>R$ 7.849,00</span>
                            <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
                        </div>
                    </article>
                </div><!--Fim do item-->
                <div class="item"><!--Item do carrosel-->
                    <article class="card-produto-home">
                        <aside class="novo">
                            <img src="assets/icons/novo.png" alt="">
                        </aside>
                        <div class="prod-home-img">
                            <a href="produto.php"><img src="assets/imgs/movel2.png" alt=""></a>
                        </div>
                        <div class="prod-home-txt">
                            <h1>Product Name</h1>
                            <p>detalhe e informações do produto</p>
                            <span>SKU 9702133</span>
                            <img src="assets/icons/estrelas.jpg" alt="">
                        </div>
                        <div class="preco-prod-home">
                            <span>R$ 7.849,00</span>
                            <a href=""><img src="assets/icons/carrinho-produto.jpg" alt=""></a>
                        </div>
                    </article>
                </div><!--Fim do item-->
            </div>
        </div>
    </section>
    <!-- FOOTER -->
    <?php require 'templates/footer.php' ?>
        
    <!-- CHAMA O JS -->
    <script src="assets/js/main.js"></script>
</body>
</html>
