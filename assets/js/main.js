//Carrosel de produtos
$('.owl-carousel').owlCarousel({
  loop: true,
  margin: 50,
  autoplay: true,
  autoplayTimeout: 2400,
  autoplayHoverPause: true,
  nav: false,
  responsive: {
    0: {
      items: 1
    },
    600: {
      items: 1
    },
    1000: {
      items: 2
    },
    1100: {
      items: 3
    }
  }
})


function myFunction() {
  var x = document.getElementById("nav");
  if (x.style.display === "flex") {
    x.style.display = "none";
  } else {
    x.style.display = "flex";
  }
}